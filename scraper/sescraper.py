import sys
import time
import os
import urlparse
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from scraper import is_top_url, is_refcard_tag, is_refcard_name_tag, Refcard

MAX_WAIT = 5

USERNAME = os.environ.get('USERNAME')
PASSWORD = os.environ.get('PASSWORD')
BASE_URL = os.environ.get('HOMEPAGE')
REFCARDS_PATH = '/tmp/refcards/'


def wait_for(condition_function):
    start_time = time.time()
    while time.time() < start_time + MAX_WAIT:
        if condition_function():
            return True
        else:
            time.sleep(0.1)
    raise Exception('Timeout waiting for {}'.format(condition_function.__name__))


class wait_for_page_load(object):
    """
    Based on http://www.obeythetestinggoat.com/how-to-get-selenium-to-wait-for-page-load-after-a-click.html
    """

    def __init__(self, browser):
        self.browser = browser

    def __enter__(self):
        self.old_page = self.browser.find_element_by_tag_name('html')

    def page_has_loaded(self):
        new_page = self.browser.find_element_by_tag_name('html')
        return new_page.id != self.old_page.id

    def __exit__(self, *_):
        wait_for(self.page_has_loaded)


def load_and_wait(browser, link=None, click_on=None):
    with wait_for_page_load(browser):
        if link:
            return browser.get(link)
        elif click_on:
            return click_on.click()


def get_top_url_ids(url, soup):
    return {link.attrs['id']: link.attrs['href'] for link in (soup.find_all(is_top_url))}


def get_refcards_link_hrefs(url, soup):
    refcards = {}
    for refcard_tag in soup.find_all(is_refcard_tag):
        try:
            href = refcard_tag.attrs['href']
            href_id = href.split('/')[-1]
            grand_parent_tag = refcard_tag.parent.parent
            refcard_name_tag = grand_parent_tag.find(is_refcard_name_tag) or \
                               grand_parent_tag.find('p', attrs={'class': 'asset-image'})
            refcard_name = refcard_name_tag.attrs['href'].split('/')[2]
            refcards[href_id] = Refcard(name=refcard_name, download_url=(urlparse.urljoin(url, href)), href=href)
        except AttributeError, e:
            continue
    return refcards


class NotFound(Exception):
    pass


def scrape():
    browser = init_browser()
    done = set()
    done_urls = set()
    url_ids = process_page(browser, done, BASE_URL)
    done_urls.add(BASE_URL)
    for href in url_ids.values():
        url = urlparse.urljoin(BASE_URL, href)
        new_url_ids = process_page(browser, done, url)
        done_urls.add(url)


def process_page(browser, done, url):
    load_and_wait(browser, link=url)
    if url == BASE_URL:
        sign_in(browser)
    html = browser.page_source
    soup = BeautifulSoup(html, 'html.parser')
    url_ids = get_top_url_ids(url, soup)
    refcards = get_refcards_link_hrefs(url, soup)
    try:
        for refcard in refcards.values():
            if not refcard.href in done:
                try:
                    print "Button: %s: %s" % (refcard.name, refcard.href)
                    actionChains = ActionChains(browser)
                    refcard_buttons = browser.find_elements_by_xpath(
                        '//a[@type="button" and @class="btn download" and @href="%s"]' % refcard.href)
                    for refcard_button in refcard_buttons:
                        grand_parent = refcard_button.find_element_by_xpath('../..')
                        browser.execute_script('arguments[0].scrollIntoView({behavior: "instant", block: "end"})',
                                               grand_parent)
                        # Hover over block
                        actionChains.move_to_element(grand_parent).perform()
                        print "Click!"
                        # Must use JS click or we'd get the Se invisible message error
                        browser.execute_script('arguments[0].click()', refcard_button)
                        done.add(refcard.href)
                except NotFound:
                    print "Not found"

    except NoSuchElementException, e:
        print 'NoSuchElementException: %s' % e

    return url_ids


def sign_in(browser):
    browser.find_element_by_xpath('//a[@ng-click="loginForm()"]').click()
    browser.find_element_by_xpath('//input[@name="username"]').send_keys(USERNAME)
    browser.find_element_by_xpath('//input[@name="password"]').send_keys(PASSWORD)
    signin_tag = browser.find_element_by_xpath('//input[@ng-click="doLogin()"]')
    with wait_for_page_load(browser):
        signin_tag.click()


def init_browser():
    # Auto save PDF files to given directory
    profile = webdriver.FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.download.manager.addToRecentDocs", False)
    profile.set_preference("browser.download.manager.useWindow", False)
    profile.set_preference("browser.download.dir", REFCARDS_PATH)
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
    profile.set_preference('pdfjs.disabled', True)
    browser = webdriver.Firefox(firefox_profile=profile)
    return browser


if __name__ == '__main__':
    scrape()
