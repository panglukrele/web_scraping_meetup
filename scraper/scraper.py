import os
import urllib2
import base64
import urlparse
from bs4 import BeautifulSoup

USERNAME = os.environ.get('USERNAME')
PASSWORD = os.environ.get('PASSWORD')
BASE_URL = os.environ.get('HOMEPAGE')
REFCARDS_PATH = '/tmp/refcards/'
MAX_RETRIES = 3
MAX_NUM = 300


class Refcard(object):
    def __init__(self, name, download_url, href=None):
        self.name = name
        self.download_url = download_url
        self.href = href


def get_url(url, retries=MAX_RETRIES):
    for i in xrange(retries):
        try:
            request = urllib2.Request(url)
            request.add_header("Authorization",
                               "Basic %s" %
                               base64.encodestring('%s:%s' % (USERNAME, PASSWORD)).replace('\n', ''))
            return urllib2.urlopen(request)
        except urllib2.URLError as e:
            if hasattr(e, 'code') and not (501 <= e.code <= 599):
                print 'Download [%s] error: %s' % (url, e.reason)
                return None
            print 'Download [%s %d/%d] error: %s' % (url, i + 1, MAX_RETRIES, e.reason)
    return None


def retrieve_file(refcard_url):
    response = get_url(refcard_url)
    if response:
        filename = urlparse.urlparse(response.url).path.split('/')[-1]
        filepath = REFCARDS_PATH + filename
        if os.path.isfile(filepath):
            print "Skipping Existing: %s: %s" % (filename, refcard_url)
        else:
            print "Downloading: %s: %s" % (filename, refcard_url)
            with open(filepath, 'wb') as handle:
                handle.write(response.read())
            return True
    return False


def is_top_url(tag):
    return tag.name == 'a' and tag.has_attr('ng-class')


def is_refcard_name_tag(tag):
    return tag.name == 'a' and tag.has_attr('href') and tag.attrs['href'].startswith('/refcardz/')


def is_refcard_tag(tag):
    return tag.name == 'a' \
           and tag.has_attr('type') and tag.attrs['type'] == 'button' \
           and tag.has_attr('class') and tag.attrs['class'] == [u'btn', u'download'] \
           and tag.has_attr('href') and tag.attrs['href'].startswith('/asset/download/')


def get_top_urls(url, soup):
    return [urlparse.urljoin(url, link.attrs['href']) for link in (soup.find_all(is_top_url))]


def get_refcards_links(url, soup):
    refcards = {}
    for refcard_tag in soup.find_all(is_refcard_tag):
        try:
            href = refcard_tag.attrs['href']
            href_id = href.split('/')[-1]
            grand_parent_tag = refcard_tag.parent.parent
            refcard_name_tag = grand_parent_tag.find(is_refcard_name_tag) or \
                               grand_parent_tag.find('p', attrs={'class': 'asset-image'})
            refcard_name = refcard_name_tag.attrs['href'].split('/')[2]
            refcards[href_id] = Refcard(name=refcard_name, download_url=(urlparse.urljoin(url, href)))
        except AttributeError, e:
            continue
    return refcards


def harvest_url(home_url):
    html = get_url(home_url).read()
    if html is not None:
        print "Retrieving download URLs from:"
        print "  %s" % home_url
        soup = BeautifulSoup(html, 'html.parser')
        urls = get_top_urls(home_url, soup)
        refcards = get_refcards_links(home_url, soup)
        for url in urls:
            print "  %s" % url
            refcards.update(get_refcards_links(url, BeautifulSoup(get_url(url), 'html.parser')))
        for refcard in refcards.values():
            retrieve_file(refcard.download_url)
        print
        download_base_url = '/'.join(refcards.values()[0].download_url.split('/')[:-1])
        for i in xrange(MAX_NUM + 1):
            if str(i) not in refcards:
                refcard_url = '%s/%d' % (download_base_url, i)
                retrieve_file(refcard_url)


if __name__ == "__main__":
    harvest_url(BASE_URL)
